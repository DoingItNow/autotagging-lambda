"""
Auto tags various AWS Resources that are set as non-complaint in AWS Config

Note: The following resources won't be tagged: ASG, RDS DB Instances, Redshift Cluster Snapshots
Reasoning:
    ASG Tagging is not supported in TaggingResourceAPI
    RDS DB Instance returns garbage resource ID that cannot be used anywhere
    Redshift Cluster Snapshots has an ARN that needs cluster-name which requires more work to fix
"""

import os
import boto3
import get_name_tags

config_service = boto3.client('config')
tagging_service = boto3.client('resourcegroupstaggingapi')
account_id = boto3.client('sts').get_caller_identity().get('Account')
region = boto3.session.Session().region_name

def create_resource_arn_dict():
    """
        Creates a dictionary with a set of key/values where
        Key: Resource Type from AWS Config
        Value: The ARN of the resource which will be used for tagging
    """
    arn_dictionary = {
        'AWS::EC2::Instance': 'arn:aws:ec2:' + region + ':' + account_id + ':instance/',
        'AWS::EC2::NetworkAcl': 'arn:aws:ec2:' + region + ':' + account_id + ':network-acl/',
        'AWS::EC2::NetworkInterface': 'arn:aws:ec2:' + region + ':' + account_id + ':network-interface/',
        'AWS::EC2::RouteTable': 'arn:aws:ec2:' + region + ':' + account_id + ':route-table/',
        'AWS::EC2::SecurityGroup': 'arn:aws:ec2:' + region + ':' + account_id + ':security-group/',
        'AWS::EC2::Volume': 'arn:aws:ec2:' + region + ':' + account_id + ':volume/',
        'AWS::EC2::CustomerGateway': 'arn:aws:ec2:' + region + ':' + account_id + ':customer-gateway/',
        'AWS::EC2::InternetGateway': 'arn:aws:ec2:' + region + ':' + account_id + ':internet-gateway/',
        'AWS::EC2::Subnet': 'arn:aws:ec2:' + region + ':' + account_id + ':subnet/',
        'AWS::EC2::VPC': 'arn:aws:ec2:' + region + ':' + account_id + ':vpc/',
        'AWS::EC2::VPNGateway': 'arn:aws:ec2:' + region + ':' + account_id + ':vpn-gateway/',
        'AWS::ElasticLoadBalancing::LoadBalancer': 'arn:aws:elasticloadbalancing:' + region + ':' + account_id + ':loadbalancer/',
        'AWS::DynamoDB::Table': 'arn:aws:dynamodb:' + region + ':' + account_id + ':table/',
        # 'AWS::RDS::DBInstance': 'arn:aws:rds:' + region + ':' + account_id + ':db:',
        'AWS::RDS::DBSnapshot': 'arn:aws:rds:' + region + ':' + account_id + ':snapshot:',
        'AWS::RDS::DBSubnetGroup': 'arn:aws:rds:' + region + ':' + account_id + ':subgrp:',
        'AWS::RDS::EventSubscription': 'arn:aws:rds:' + region + ':' + account_id + ':es:',
        'AWS::Redshift::Cluster': 'arn:aws:redshift:' + region + ':' + account_id + ':cluster:',
        # 'AWS::Redshift::ClusterSnapshot': 'arn:aws:redshift:' + region + ':' + account_id + 'snapshot:'
        'AWS::Redshift::ClusterParameterGroup': 'arn:aws:redshift:' + region + ':' + account_id + ':parametergroup:',
        'AWS::Redshift::ClusterSecurityGroup': 'arn:aws:redshift:' + region + ':' + account_id + ':securitygroup:',
        'AWS::Redshift::ClusterSubnetGroup': 'arn:aws:redshift:' + region + ':' + account_id + ':subnetgroup:',
        'AWS::S3::Bucket': 'arn:aws:s3:::'
    }
    return  arn_dictionary

def create_application_tags_dict():
    """
        Creates a dictionary with a set of key/values where
        Key: Resource Type from AWS Config
        Value: The application tag value
    """
    app_tags_dictionary = {
        'AWS::EC2::Instance': 'EC2',
        'AWS::EC2::NetworkAcl': 'VPC',
        'AWS::EC2::NetworkInterface': 'VPC',
        'AWS::EC2::RouteTable': 'VPC',
        'AWS::EC2::SecurityGroup': 'VPC',
        'AWS::EC2::Volume': 'EC2',
        'AWS::EC2::CustomerGateway': 'VPC',
        'AWS::EC2::InternetGateway': 'VPC',
        'AWS::EC2::Subnet': 'VPC',
        'AWS::EC2::VPC': 'VPC',
        'AWS::EC2::VPNGateway': 'VPC',
        'AWS::ElasticLoadBalancing::LoadBalancer': 'ELB',
        'AWS::ElasticLoadBalancingV2::LoadBalancer': 'ELBv2',
        'AWS::DynamoDB::Table': 'DynamoDB',
        # 'AWS::RDS::DBInstance': 'RDS',
        'AWS::RDS::DBSnapshot': 'RDS',
        'AWS::RDS::DBSubnetGroup': 'RDS',
        'AWS::RDS::EventSubscription': 'RDS',
        'AWS::Redshift::Cluster': 'Redshift',
        # 'AWS::Redshift::ClusterSnapshot': 'Redshift',
        'AWS::Redshift::ClusterParameterGroup': 'Redshift',
        'AWS::Redshift::ClusterSecurityGroup': 'Redshift',
        'AWS::Redshift::ClusterSubnetGroup': 'Redshift',
        'AWS::S3::Bucket': 'S3'
    }
    return app_tags_dictionary

def create_name_tags_function():
    """
        Creates a dictionary with a set of key/values where
        Key: Resource Type from AWS Config
        Value: The function to create name tags
    """
    name_functions_dictionary = {
        # 'AWS::AutoScaling::AutoScalingGroup': get_name_tags.get_autoscale_name_tag(single_resource_id),
        'AWS::EC2::Instance': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::NetworkAcl': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::NetworkInterface': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::RouteTable': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::SecurityGroup': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::Volume': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::CustomerGateway': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id',
        'AWS::EC2::InternetGateway': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::Subnet': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::VPC': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::EC2::VPNGateway': 'get_name_tags.get_ec2_resource_name_tag(single_resource_id)',
        'AWS::ElasticLoadBalancing::LoadBalancer': 'get_name_tags.get_elb_name_tag(single_resource_id)',
        'AWS::ElasticLoadBalancingV2::LoadBalancer': 'get_name_tags.get_elbv2_name_tag(single_resource_id)',
        'AWS::DynamoDB::Table': 'get_name_tags.get_dynamodb_name_tag(single_resource_id)',
        # 'AWS::RDS::DBInstance': 'get_name_tags.get_rds_instance_name_tag(single_resource_id)',
        'AWS::RDS::DBSnapshot': 'get_name_tags.get_rds_snapshot_name_tag(single_resource_id)',
        'AWS::RDS::DBSubnetGroup': 'get_name_tags.get_rds_subnet_group_name_tag(single_resource_id)',
        'AWS::RDS::EventSubscription': 'get_name_tags.get_rds_event_sub_name_tag(single_resource_id)',
        'AWS::Redshift::Cluster': 'get_name_tags.get_redshift_cluster_name_tag(single_resource_id)',
        'AWS::Redshift::ClusterParameterGroup': 'get_name_tags.get_redshift_param_name_tag(single_resource_id)',
        'AWS::Redshift::ClusterSecurityGroup': 'get_name_tags.get_redshift_security_name_tag(single_resource_id)',
        'AWS::Redshift::ClusterSubnetGroup': 'get_name_tags.get_redshift_subnet_name_tag(single_resource_id)',
        'AWS::S3::Bucket': 'get_name_tags.get_s3_bucket_name_tag(single_resource_id)',
    }
    return name_functions_dictionary

def create_non_compliant_resources_list():
    """
        Gets all the non-compliant resources from AWS Config and saves
        the resource_type and resource_id as a list of lists
    """
    resources_list = []
    non_compliant_resources_blob = config_service.get_compliance_details_by_config_rule(ConfigRuleName="RequiredTagsDefined", ComplianceTypes=['NON_COMPLIANT'], Limit=100)

    non_compliant_resource_list = non_compliant_resources_blob['EvaluationResults']
    for resource in non_compliant_resource_list:
        single_resource_list = []

        resource_type = resource['EvaluationResultIdentifier']['EvaluationResultQualifier']['ResourceType']
        resource_id = resource['EvaluationResultIdentifier']['EvaluationResultQualifier']['ResourceId']

        single_resource_list.append(resource_type)
        single_resource_list.append(resource_id)
        resources_list.append(single_resource_list)

    return resources_list

def tag_non_compliant_resources(non_compliant_lists, resource_arns_dict, app_tags_dict):
    """
        Tags the non-compliant resources based of the non_compliant_lists
        Name tags are set from the name tags dictionary
    """
    for resource_list in non_compliant_lists:
        single_resource_type = resource_list[0]
        single_resource_id = resource_list[1]
        new_name_tag_dictionary = create_name_tags_function()

        if single_resource_type in app_tags_dict:

            # Note that single_resource_id is tightly coupled to the dictionary
            name_tag_function = new_name_tag_dictionary[single_resource_type]
            new_name_tag= eval(name_tag_function)

            new_resource_tags = {
                'Name': new_name_tag,
                'Environment': os.environ.get('ENVIRONMENT'),
                'Service': os.environ.get('SERVICE'),
                'Application': app_tags_dict[single_resource_type],
                'Owner': os.environ.get('OWNER'),
                'ManagedBy': os.environ.get('MANAGEDBY')
            }

            # Certain AWS Config Resource IDs will return an actual ARN. If thats the case the
            # resource_arn_list will tbe resource ID itself
            if single_resource_type in resource_arns_dict:
                resource_arn = resource_arns_dict[single_resource_type] + single_resource_id
                resource_arn_list = [resource_arn]
            else:
                resource_arn_list = [single_resource_id]

            tagging_response = tagging_service.tag_resources(ResourceARNList=resource_arn_list, Tags=new_resource_tags)

            # If the tagging_response dictionary is not empty
            if tagging_response['FailedResourcesMap']:
                print(tagging_response['FailedResourcesMap'][resource_arn_list[0]]['ErrorMessage'])
            else:
                print("Tagging resource " + single_resource_type + " called " + single_resource_id + " with compliant tags")
        else:
            print("Resource type " +  single_resource_type + " for " + single_resource_id + " is currently not supported. Skipping")

def lambda_handler(event, context):
    """
        Lambda handler to run main()
    """
    print('lambda function started')
    main()

def main():
    """
        Gets the resource ARNs, App Tags and Non-Compliant Resource dictionaries and
        set the resources to have the proper tags
    """
    resource_arn_dict = create_resource_arn_dict()
    app_tags_dict = create_application_tags_dict()
    non_compliant_resource_lists = create_non_compliant_resources_list()

    tag_non_compliant_resources(non_compliant_resource_lists, resource_arn_dict, app_tags_dict)

if __name__ == '__main__':
    main()
