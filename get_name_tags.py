"""
Module to set name tags for various resources
"""

import boto3
import botocore
def create_default_ec2_resource_name(resource_id):
    """
        Creates a dictionary with a set of key/values where
        Key: Resource Type from AWS Config
        Value: The name tag value of the new resource
    """
    default_name_tags_dictionary = {
        'AWS::EC2::Instance': 'autotagged-instance-' + resource_id,
        'AWS::EC2::NetworkAcl': 'autotagged-nacl-' + resource_id,
        'AWS::EC2::NetworkInterface': 'autotagged-' + resource_id,
        'AWS::EC2::RouteTable': 'autotagged-' + resource_id,
        'AWS::EC2::SecurityGroup': 'autotagged-' + resource_id,
        'AWS::EC2::Volume': 'autotagged-' + resource_id,
        'AWS::EC2::CustomerGateway': 'autotagged-' + resource_id,
        'AWS::EC2::InternetGateway': 'autotagged-' + resource_id,
        'AWS::EC2::Subnet': 'autotagged-' + resource_id,
        'AWS::EC2::VPC': 'autotagged-' + resource_id,
        'AWS::EC2::VPNGateway': 'autotagged-' + resource_id
    }

    return default_name_tags_dictionary

def get_ec2_resource_name_tag(resource_type, resource_id):
    """
       Checks the EC2 Resource type for a Name tag. If it exists, reuse else use
       the name from default_names_dictionary
    """
    ec2_service = boto3.client('ec2')

    name_tag_exists = False
    name_tag = ""

    try:
        default_names_dict = create_default_ec2_resource_name(resource_id)
        resource_tags = ec2_service.describe_tags(DryRun=False, Filters=[{'Name': 'resource-id', 'Values': [resource_id]}])['Tags']

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag_exists = True
                name_tag = resource_tags[tag_index]['Value']

        if not name_tag_exists and resource_type in default_names_dict:
            name_tag = default_names_dict[resource_type]
    except ec2_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_elb_name_tag(resource_id):
    """
       Checks the ELB Resource type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    elb_service = boto3.client('elb')
    name_tag = resource_id

    try:
        resource_tags = elb_service.describe_tags(LoadBalancerNames=[resource_id])["TagDescriptions"][0]["Tags"]
        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']

    except elb_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_elbv2_name_tag(resource_id):
    """
       Checks the ELBv2 (ALBs, NLBs) Resource type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    elbv2_service = boto3.client('elbv2')

    name_tag_exists = False
    name_tag = ""

    try:
        loadbalancer_name = elbv2_service.describe_load_balancers(LoadBalancerArns=[resource_id])['LoadBalancers'][0]['LoadBalancerName']

        loadbalancer_arn = elbv2_service.describe_load_balancers(LoadBalancerArns=[resource_id])['LoadBalancers'][0]['LoadBalancerArn']
        resource_tags = elbv2_service.describe_tags(ResourceArns=[loadbalancer_arn])["TagDescriptions"][0]["Tags"]

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag_exists = True
                name_tag = resource_tags[tag_index]['Value']

        if not name_tag_exists:
            name_tag = loadbalancer_name
    except elbv2_service.exceptions.ClientError as e: 
        print(e)

    return name_tag


def get_dynamodb_name_tag(resource_id):
    """
       Checks the DynamoDB Resource type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    dynamodb_service = boto3.client('dynamodb')
    name_tag = resource_id

    try:
        table_arn = dynamodb_service.describe_table(TableName=resource_id)['Table']['TableArn']
        resource_tags = dynamodb_service.list_tags_of_resource(ResourceArn=table_arn)["Tags"]

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']

    except dynamodb_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_rds_snapshot_name_tag(resource_id):
    """
       Checks the RDS Snapshot Resource type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    rds_service = boto3.client('rds')
    name_tag = resource_id

    try:
        snapshot_instance_arn = rds_service.describe_db_snapshots(DBSnapshotIdentifier=resource_id)['DBSnapshots'][0]['DBSnapshotArn']
        resource_tags = rds_service.list_tags_for_resource(ResourceName=snapshot_instance_arn)["TagList"]

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']

    except rds_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_rds_subnet_group_name_tag(resource_id):
    """
       Checks the RDS Subnet Group Resource type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    rds_service = boto3.client('rds')
    name_tag = resource_id

    try:
        db_subnet_group_arn = rds_service.describe_db_subnet_groups(DBSubnetGroupName=resource_id)['DBSubnetGroups'][0]['DBSubnetGroupArn']
        resource_tags = rds_service.list_tags_for_resource(ResourceName=db_subnet_group_arn)["TagList"]

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']
    except rds_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_rds_event_sub_name_tag(resource_id):
    """
       Checks the RDS Event Subscription Resource type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    rds_service = boto3.client('rds')
    name_tag = resource_id

    try:
        event_sub_arn = rds_service.describe_event_subscriptions(SubscriptionName=resource_id)['EventSubscriptionsList'][0]['EventSubscriptionArn']
        resource_tags = rds_service.list_tags_for_resource(ResourceName=event_sub_arn)["TagList"]

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']
    except rds_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_redshift_cluster_name_tag(resource_id):
    """
       Checks the Redshift Cluster Resource type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    redshift_service = boto3.client('redshift')
    name_tag = resource_id

    try:
        resource_tags = redshift_service.describe_clusters(ClusterIdentifier=resource_id)['Clusters'][0]['Tags']

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']
    except redshift_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_redshift_param_name_tag(resource_id):
    """
       Checks the Redshift Parameter Group type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    redshift_service = boto3.client('redshift')
    name_tag = resource_id

    try:
        resource_tags = redshift_service.describe_cluster_parameter_groups(ParameterGroupName=resource_id)['ParameterGroups'][0]['Tags']

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']
    except redshift_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_redshift_subnet_name_tag(resource_id):
    """
       Checks the Redshift Subnet Group type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    redshift_service = boto3.client('redshift')
    name_tag = resource_id

    try:
        resource_tags = redshift_service.describe_cluster_subnet_groups(ClusterSubnetGroupName=resource_id)['ClusterSubnetGroups'][0]['Tags']

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']
    except redshift_service.exceptions.ClientError as e:
        print(e)
    return name_tag

def get_redshift_security_name_tag(resource_id):
    """
       Checks the Redshift Security Group type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    redshift_service = boto3.client('redshift')
    name_tag = resource_id

    try:
        resource_tags = redshift_service.describe_cluster_security_groups(ClusterSecurityGroupName=resource_id)['ClusterSecurityGroups'][0]['Tags']

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']
    except redshift_service.exceptions.ClientError as e:
        print(e)

    return name_tag

def get_s3_bucket_name_tag(resource_id):
    """
       Checks the S3 Bucket type for a Name tag. If it exists, reuse the Name Tag else use
       the resource_id as the name
    """
    s3_service = boto3.client('s3')
    name_tag = resource_id

    try:
        resource_tags = s3_service.get_bucket_tagging(Bucket=resource_id)['TagSet']

        for tag_index in range(len(resource_tags)):
            if "Name" in resource_tags[tag_index]["Key"]:
                name_tag = resource_tags[tag_index]['Value']
    except (s3_service.exceptions.ClientError, botocore.exceptions.ParamValidationError) as e:
        print(e)                                                  

    return name_tag

## Uncomment once ASG is available resource in TaggingResourceAPI
# def get_autoscale_name_tag(resource_id):
#     autoscaling_service = boto3.client('autoscaling')
#     name_tag_exists = False
#     name_tag = ""
#     asg_name = ""

#     try:
#         autoscaling_group_list = autoscaling_service.describe_auto_scaling_groups()['AutoScalingGroups']
#         for asg in autoscaling_group_list:
#             if asg['AutoScalingGroupARN'] == resource_id:
#                 asg_name = asg['AutoScalingGroupName']

#         resource_tags = autoscaling_service.describe_tags(Filters=[{'Name': 'auto-scaling-group', 'Values': [asg_name]}])['Tags']

#         for tag_index in range(len(resource_tags)):
#             if "Name" in resource_tags[tag_index]["Key"]:
#                 name_tag_exists = True
#                 name_tag = resource_tags[tag_index]['Value']

#         if not name_tag_exists:
#             name_tag = asg_name
#     except autoscaling_service.exceptions.ClientError:
#         pass

#     return name_tag

## Uncomment once RDS Resource ID is in a proper state
# def get_rds_instance_name_tag(resource_id):
#     rds_service = boto3.client('rds')
#     name_tag = resource_id

#     try:
#         db_instance_arn = rds_service.describe_db_instances(DBInstanceIdentifier=resource_id)['DBInstances'][0]['DBInstanceArn']
#         resource_tags = rds_service.list_tags_for_resource(ResourceName=db_instance_arn)["TagList"]

#         for tag_index in range(len(resource_tags)):
#             if "Name" in resource_tags[tag_index]["Key"]:
#                 name_tag = resource_tags[tag_index]['Value']
#     except rds_service.exceptions.ClientError:
#         pass

#     return name_tag
